import {MigrationInterface, QueryRunner} from "typeorm";

export class init1617375185901 implements MigrationInterface {
    name = 'init1617375185901'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`UPDATE public.department SET "managerId"=1 WHERE id =1 ;`);
        await queryRunner.query(`UPDATE public.department SET "managerId"=5 WHERE id =2 ;`);
        await queryRunner.query(`UPDATE public.department SET "managerId"=11 WHERE id =3 ;`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`TRUNCATE "department"`);
        await queryRunner.query(`DELETE FROM "department"`);
    }

}
