import {MigrationInterface, QueryRunner} from "typeorm";

export class init1617375185899 implements MigrationInterface {
    name = 'init1617375185899'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`INSERT INTO public.department(name, description) VALUES ('Nhân sự', 'Nhân sự');`);
        await queryRunner.query(`INSERT INTO public.department(name, description) VALUES ('IT', 'IT');`);
        await queryRunner.query(`INSERT INTO public.department(name, description) VALUES ('Quản lý', 'Quản lý');`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`TRUNCATE "department"`);
        await queryRunner.query(`DELETE FROM "department"`);
    }

}
