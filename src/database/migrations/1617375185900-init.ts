import {MigrationInterface, QueryRunner} from "typeorm";

export class init1617375185900 implements MigrationInterface {
    name = 'init1617375185900'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`INSERT INTO public.users(name, age, sex, "departmentId") VALUES ('khoa', 22, 'male', 1);`);
        await queryRunner.query(`INSERT INTO public.users(name, age, sex, "departmentId") VALUES ('cuong', 22, 'female', 1);`);
        await queryRunner.query(`INSERT INTO public.users(name, age, sex, "departmentId") VALUES ('thien', 22, 'male', 1);`);
        await queryRunner.query(`INSERT INTO public.users(name, age, sex, "departmentId") VALUES ('a', 22, 'male', 1);`);
        await queryRunner.query(`INSERT INTO public.users(name, age, sex, "departmentId") VALUES ('b', 22, 'female', 2);`);
        await queryRunner.query(`INSERT INTO public.users(name, age, sex, "departmentId") VALUES ('c', 22, 'female', 2);`);
        await queryRunner.query(`INSERT INTO public.users(name, age, sex, "departmentId") VALUES ('d', 22, 'male', 2);`);
        await queryRunner.query(`INSERT INTO public.users(name, age, sex, "departmentId") VALUES ('e', 22, 'female', 3);`);
        await queryRunner.query(`INSERT INTO public.users(name, age, sex, "departmentId") VALUES ('f', 22, 'male', 3);`);
        await queryRunner.query(`INSERT INTO public.users(name, age, sex, "departmentId") VALUES ('g', 22, 'male', 3);`);
        await queryRunner.query(`INSERT INTO public.users(name, age, sex, "departmentId") VALUES ('z', 22, 'male', 3);`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`TRUNCATE "users"`);
        await queryRunner.query(`DELETE FROM "users"`);
    }

}
