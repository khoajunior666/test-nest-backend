import {MigrationInterface, QueryRunner} from "typeorm";

export class init1617375185898 implements MigrationInterface {
    name = 'init1617375185898'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "users" ("id" SERIAL NOT NULL, "name" character varying(50) NOT NULL, "age" integer NOT NULL, "sex" character varying(100), "departmentId" integer, CONSTRAINT "PK_a3ffb1c0c8416b9fc6f907b7433" PRIMARY KEY ("id"))`);
        await queryRunner.query(`ALTER TABLE "department" ADD "description" character varying(500) NOT NULL`);
        await queryRunner.query(`ALTER TABLE "department" ADD "managerId" integer`);
        await queryRunner.query(`ALTER TABLE "department" ADD CONSTRAINT "UQ_2147eb9946aa96094b7f78b1954" UNIQUE ("managerId")`);
        await queryRunner.query(`ALTER TABLE "users" ADD CONSTRAINT "FK_554d853741f2083faaa5794d2ae" FOREIGN KEY ("departmentId") REFERENCES "department"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "department" ADD CONSTRAINT "FK_2147eb9946aa96094b7f78b1954" FOREIGN KEY ("managerId") REFERENCES "users"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "department" DROP CONSTRAINT "FK_2147eb9946aa96094b7f78b1954"`);
        await queryRunner.query(`ALTER TABLE "users" DROP CONSTRAINT "FK_554d853741f2083faaa5794d2ae"`);
        await queryRunner.query(`ALTER TABLE "department" DROP CONSTRAINT "UQ_2147eb9946aa96094b7f78b1954"`);
        await queryRunner.query(`ALTER TABLE "department" DROP COLUMN "managerId"`);
        await queryRunner.query(`ALTER TABLE "department" DROP COLUMN "description"`);
        await queryRunner.query(`DROP TABLE "users"`);
    }

}
