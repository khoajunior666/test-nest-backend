import {MigrationInterface, QueryRunner} from "typeorm";

export class init1617289737691 implements MigrationInterface {
    name = 'init1617289737691'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "department" ("id" SERIAL NOT NULL, "name" character varying(50) NOT NULL, CONSTRAINT "PK_9a2213262c1593bffb581e382f5" PRIMARY KEY ("id"))`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`DROP TABLE "dog"`);
        await queryRunner.query(`DROP TABLE "department"`);
    }

}
