import { EntityRepository, Repository } from 'typeorm';
import {DepartmentDto} from './department.dto'
import {Department} from './department.entity'


@EntityRepository(Department)
export class DepartmentRepository extends Repository<Department> {
  createDepartment = async (departmentDto: DepartmentDto) => {
    return await this.save(departmentDto)
  }

  findOneDepartment = async (id:string) => {
    return this.findOneOrFail(id)
  }

}
