import { Entity, PrimaryGeneratedColumn, Column, OneToMany, OneToOne, JoinColumn } from 'typeorm';
import {Users} from '../users/users.entity'


@Entity()
export class Department {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({length: 50})
  name: string;

  @Column({length: 500})
  description: string;

  @OneToMany(type => Users, users => users.department)
  listEmployee: Users[];

  @OneToOne(() => Users)
  @JoinColumn()
  manager: Users;



}
