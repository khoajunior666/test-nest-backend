import { Injectable } from '@nestjs/common';
import { Department } from './department.entity';
import { DepartmentDto } from './department.dto';
import { from } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { DepartmentRepository } from './department.repository';
import { createQueryBuilder } from 'typeorm';
import { Users } from '../users/users.entity';

@Injectable()
export class DepartmentService {

  constructor(
   // @InjectRepository(Department) private departmentRepository: Repository<Department>,
    private departmentRepository: DepartmentRepository
  ) {}


  findAll() {
    return this.departmentRepository.find()
  }

  findOne(id: number){
    return this.departmentRepository.findOne(id)
  }

  create(body: DepartmentDto) {
    const newDepartment = new Department();
    newDepartment.name = body.name;
    newDepartment.description = body.description;
    return this.departmentRepository.save(newDepartment);
  }

  add(body: Department) {
    return this.departmentRepository.save(body);
  }


  updateDepartment(department: Department){
    return from(this.departmentRepository.update(department.id, department)).pipe(
      switchMap(() => this.findOne(department.id))
    )
  }


   async getAllDepartment() {
     return await this.departmentRepository.query(`SELECT * FROM department;`)
  }

  async getOneDepartment(id: number){
    return await this.departmentRepository.findOne(
      {relations: ["manager"],
              where: {id: id}
      }
      )
  }

  async getOneDepartmentByName(name: string){
    return await this.departmentRepository.findOne(
      {relations: ["manager"],
        where: {name: name}
      }
    )
  }




}
