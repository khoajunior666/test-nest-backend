import { Controller, Get, Post, Body, Put, Param, Delete } from '@nestjs/common';
import {DepartmentDto} from './department.dto'
import {Department} from './department.entity'
import { InjectRepository } from '@nestjs/typeorm';
import {DepartmentService} from './department.service'


@Controller('api/v1/department')
export class DepartmentController {

  constructor(
    private departmentService: DepartmentService
  ) {}


  @Get()
  getDepartment(){
    return this.departmentService.findAll();
  }


  @Get(':id')
  findOne(@Param('id') id: number) {
    return this.departmentService.findOne(id);
  }


  @Post()
  create(@Body() body: DepartmentDto) {
    return this.departmentService.create(body);
  }
}
