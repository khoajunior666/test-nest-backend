export class DepartmentDto {
  readonly name: string;
  readonly description: string;
}
