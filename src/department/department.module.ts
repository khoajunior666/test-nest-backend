import { Module } from '@nestjs/common';
import { DepartmentService } from './department.service';
import { DepartmentController } from './department.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import {Department} from './department.entity'
import {DepartmentRepository} from './department.repository'


@Module({
  imports: [
    TypeOrmModule.forFeature([Department,DepartmentRepository]),
  ],
  providers: [DepartmentService],
  controllers: [DepartmentController],
  exports: [DepartmentService]
})
export class DepartmentModule {}
