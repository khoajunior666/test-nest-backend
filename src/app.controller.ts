import { Controller, Get } from '@nestjs/common';
import {UsersService} from './users/users.service'
import {DepartmentService} from './department/department.service'
import {DepartmentDto} from './department/department.dto'
import {UsersDto} from './users/users.dto'
import {Department} from './department/department.entity'
import {Users} from './users/users.entity'

@Controller()
export class AppController {
  constructor(
    private usersService:UsersService,
    private departmentService:DepartmentService
  ) {}

  @Get()
  getHello(): string {
    return "hello";
  }

  @Get('/add-data')
  async addData(): Promise<string> {
    const users = await this.usersService.findAll();
    const departments = await this.departmentService.findAll();
    if(users.length > 0 || departments.length > 0){
      console.log("có data")
    }else {
      let department = new Department()
      let department2 = new Department()
      let department3 = new Department()
      department.name = 'phòng nhân sự'
      department.description = 'phòng nhân sự'
      await this.departmentService.add(department)
      department2.name = 'phòng IT'
      department2.description = 'IT'
      await this.departmentService.add(department2)
      department3.name = 'phòng quản lý'
      department3.description = 'phòng quản lý'
      await this.departmentService.add(department3)
      console.log("chưa có data")
      const nhanSu =await this.departmentService.getOneDepartmentByName("phòng nhân sự")
      const IT =await this.departmentService.getOneDepartmentByName("phòng IT")
      const quanLy =await this.departmentService.getOneDepartmentByName("phòng quản lý")
      let user = new Users()
      user.name = 'khoa'
      user.age = 22
      user.sex = 'male'
      user.department = nhanSu
      await this.usersService.add(user)
      let user2 = new Users()
      user2.name = 'Cường'
      user2.age = 22
      user2.sex = 'female'
      user2.department = nhanSu
      await this.usersService.add(user2)
      let user3 = new Users()
      user3.name = 'b'
      user3.age = 39
      user3.sex = 'male'
      user3.department = nhanSu
      await this.usersService.add(user3)
      let user4 = new Users()
      user4.name = 'c'
      user4.age = 40
      user4.sex = 'male'
      user4.department = IT
      await this.usersService.add(user4)
      let user5 = new Users()
      user5.name = 'D'
      user5.age = 45
      user5.sex = 'female'
      user5.department = IT
      await this.usersService.add(user5)
      let user6 = new Users()
      user6.name = 'f'
      user6.age = 20
      user6.sex = 'male'
      user6.department = IT
      await this.usersService.add(user6)
      let user7 = new Users()
      user7.name = 'e'
      user7.age = 22
      user7.sex = 'female'
      user7.department = IT
      await this.usersService.add(user7)
      let user8 = new Users()
      user8.name = 'g'
      user8.age = 26
      user8.sex = 'male'
      user8.department = quanLy
      await this.usersService.add(user7)
      let user9 = new Users()
      let user10 = new Users()
      let user12 = new Users()
      let user13 = new Users()
      let user14 = new Users()
      let user15 = new Users()
    }

    return "đã add dữ liệu";
  }
}
