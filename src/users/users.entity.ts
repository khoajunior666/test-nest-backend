import { Entity, PrimaryGeneratedColumn, Column, ManyToOne, OneToOne, JoinColumn } from 'typeorm';
import {Department} from '../department/department.entity'

@Entity()
export class Users {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ length: 50 })
  name: string;

  @Column()
  age: number;

  @Column({ length: 100, nullable: true })
  sex: string;

  @ManyToOne(type => Department,department => department.listEmployee )
  department: Department
}
