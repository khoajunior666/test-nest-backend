import { Body, Controller, Get, Param, Post, Put } from '@nestjs/common';
import {UsersService} from './users.service'
import {UsersDto,AddManager} from './users.dto'
import {DepartmentService} from '../department/department.service'

@Controller('api/v1/users')
export class UsersController {
  constructor(
    private usersService: UsersService,
  ) {}

  @Get()
  getUsers(){
    return this.usersService.findAll();
  }

  @Get('statistical')
  getStatisticalUsers(){
    return this.usersService.getStatistical();
  }

  @Get('statistical/:id')
  getStatisticalUsersDepartment(@Param('id') id: number){
    return this.usersService.getStatisticalOneDepartment(id);
  }

  @Get(':id')
  findOne(@Param('id') id: number) {
    return this.usersService.findOne(id);
  }



  @Post()
  create(@Body() body: UsersDto) {
    return this.usersService.create(body);
  }

  @Put('manage')
  addNewManage(@Body() body: AddManager){
    return this.usersService.addDepartmentManage(body)
  }


  @Put('department')
  addDepartmentToUser(@Body() body: AddManager){
    return this.usersService.addDepartmentToUser(body)
  }






}
