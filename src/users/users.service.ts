import { Injectable } from '@nestjs/common';
import { getRepository } from 'typeorm';
import { Users } from './users.entity';
import { AddManager, UsersDto, statisticalSex } from './users.dto';
import { DepartmentService } from '../department/department.service';
import { UsersRepository } from './users.repository';


@Injectable()
export class UsersService {
  constructor(
    // @InjectRepository(Users) private UserRepo: Repository<Users>,
    private departmentService: DepartmentService,
    private UserRepo: UsersRepository
  ) {}

  findAll() {
    return this.UserRepo.find()
  }

  findOne(id: number) {
    return this.UserRepo.findOne(id)
  }

  async create(body: UsersDto) {
    const newUser = new Users();
    newUser.name = body.name;
    newUser.age = body.age;
    newUser.sex = body.sex;
    const department = await this.departmentService.findOne(body.departmentId)
    console.log(department)
    newUser.department = department
    return this.UserRepo.save(newUser);
  }

  async add(body: Users) {
    return this.UserRepo.save(body);
  }

  async addDepartmentManage (addManager: AddManager) {
    const department = await this.departmentService.findOne(addManager.departmentId)
    department.manager =await this.findOne(addManager.userId)
    return this.departmentService.updateDepartment(department)
  }

  async addDepartmentToUser (addManager: AddManager) {
    const department = await this.departmentService.findOne(addManager.departmentId)
    const user = await this.findOne(addManager.userId);
    user.department = department
    return this.UserRepo.update(user.id,user)
  }


  async getStatistical () {
    // const test = await this.departmentService.getAllDepartment()
    // console.log(test)
    const departments = await this.departmentService.getAllDepartment();
    const listStatisticals = []
    for (const department of departments) {
      let females = await this.getCountSex(department.id,"female");
      let males = await this.getCountSex(department.id, "male");
      let total = await this.getTotalUser(department.id)
      console.log(department.managerId)
      let managae = await this.findOne(department.managerId)
      console.log(managae)
      console.log(department)
      let statistical = new statisticalSex()
      statistical.id = department.id
      statistical.name = department.name
      statistical.female = females
      statistical.male = males
      statistical.total = total
      statistical.manage = managae.name
      listStatisticals.push(statistical)
    }
    return listStatisticals
  }


   async getStatisticalOneDepartment(id: number) {
     const department = await this.departmentService.getOneDepartment(id);
     console.log(department)
     if (department === undefined) {
       return {
         error: "department exits"
       }
     }
     let females = await this.getCountSex(department.id,"female");
     let males = await this.getCountSex(department.id, "male");
     let total = await this.getTotalUser(department.id)
     let managae = await this.findOne(department.manager.id)
     let statistical = new statisticalSex()
     statistical.id = department.id
     statistical.name = department.name
     statistical.female = females
     statistical.male = males
     statistical.total = total
     statistical.manage = managae.name
     return statistical
   }

  async getCountSex (idDepartment: number, sex: string) {
    return await getRepository(Users)
      .createQueryBuilder("users")
      .select("users.id")
      .where("users.department.id = :id", { id: idDepartment })
      .andWhere("users.sex = :sex", { sex: sex })
      .getCount()
  }

  async getTotalUser (idDepartment: number){
    return await getRepository(Users)
      .createQueryBuilder("users")
      .select("users.id")
      .where("users.department.id = :id", { id: idDepartment })
      .getCount()
  }


}
