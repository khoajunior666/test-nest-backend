import { Module } from '@nestjs/common';
import { UsersController } from './users.controller';
import { UsersService } from './users.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import {Users} from './users.entity'
import {DepartmentModule} from '../department/department.module'


@Module({
  imports: [
    TypeOrmModule.forFeature([Users]),
    DepartmentModule,
  ],
  controllers: [UsersController],
  providers: [UsersService],
  exports: [UsersService]
})
export class UsersModule {}
