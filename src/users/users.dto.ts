export class UsersDto {
  readonly name: string;
  readonly age: number;
  readonly sex: string;
  readonly departmentId: number;
}

export class AddManager{
  readonly departmentId: number;
  readonly userId: number;
}


export class statisticalSex {
  id: number;
  name: string;
  female: number;
  male: number;
  total: number;
  manage: string;
}
